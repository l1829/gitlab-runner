# Gitlab runner


### Install Gitlab runner

[Install GitLab Runner](https://docs.gitlab.com/runner/install/)

[Install GitLab Runner on Windows](https://docs.gitlab.com/runner/install/windows.html)

#### 1. Create GitLab-Runner Folder

```cmd
mkdir C:\GitLab-Runner
cd C:\GitLab-Runner
```

#### 2. Download gitlab-runner.exe

Download the binary for [64-bit](https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-amd64.exe) or [32-bit](https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-386.exe) 

```cmd
Invoke-WebRequest -Uri "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-386.exe" -OutFile "gitlab-runner.exe"
# or
wget https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-386.exe -O gitlab-runner.exe
```

#### 3. Install GitLab Runner as a service and start it

Run an elevated command prompt maybe Powershell or CMD as Administrator

```cmd
.\gitlab-runner.exe install
.\gitlab-runner.exe start
.\gitlab-runner.exe --version
```

Output

```cmd
PS C:\projectdev\gitlab-runner> .\gitlab-runner.exe install
Runtime platform                                    arch=386 os=windows pid=5668 revision=5316d4ac version=14.6.0

Version:      14.6.0
Git revision: 5316d4ac
Git branch:   14-6-stable
GO version:   go1.13.8
Built:        2021-12-17T17:35:49+0000
OS/Arch:      windows/386

```

#### 4. Command to register runner


```cmd
./gitlab-runner.exe register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN
```

Enter setting

```cmd
Enter the GitLab instance URL (for example, https://gitlab.com/):
[https://gitlab.com/]:
Enter the registration token:
[xxx]: xxx
Enter a description for the runner:
[Dell Windows 10 for electron-js]:
Enter tags for the runner (comma-separated):
win-10,win-nodejs
Registering runner... succeeded                     runner=1sDpR1es
Enter an executor: docker-windows, docker-ssh, ssh, docker+machine, docker-ssh+machine, kubernetes, custom, docker, parallels, shell, virtualbox:
shell
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

##### Update text in config.toml

```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "DESKTOP-68AE4R1"
  url = "https://gitlab.com/"
  token = "xxx"
  executor = "shell"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
```

### Update

```cmd
.\gitlab-runner.exe uninstall
.\gitlab-runner.exe stop
.\gitlab-runner.exe install
.\gitlab-runner.exe start
```

### Uninstall

```cmd
.\gitlab-runner.exe stop
.\gitlab-runner.exe uninstall
```



